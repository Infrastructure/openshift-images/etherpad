FROM node:14-slim

ENV ETHERPAD_VERSION 1.8.17
ENV NODE_ENV production

RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
    ca-certificates curl unzip mariadb-client libreoffice git && \
    rm -r /var/lib/apt/lists/* /var/cache/apt/*
RUN update-ca-certificates

RUN cd /opt && curl -SL https://github.com/ether/etherpad-lite/archive/${ETHERPAD_VERSION}.zip -o etherpad.zip && \
    unzip etherpad.zip && rm -f etherpad.zip && \
    mv etherpad-lite-${ETHERPAD_VERSION} etherpad && \
    groupadd etherpad -g 1000870000 && \
    useradd etherpad -g 1000870000 -u 1000870000 -r -l -M -d /opt/etherpad && \
    chown -R etherpad:etherpad /opt/etherpad

ADD --chown=etherpad:etherpad theme /opt/etherpad/src/static/skins/gnome
ADD --chown=etherpad:etherpad settings.json /opt/etherpad/settings.json
ADD entrypoint.sh /entrypoint.sh

USER etherpad
WORKDIR /opt/etherpad
RUN bin/installDeps.sh && \
    npm install \
      ep_align \
      ep_spellcheck \
      ep_headings2 \
      ep_font_color \
      ep_set_title_on_pad \
      ep_markdown && \
    rm -rf ~/.npm/_cacache

USER etherpad
EXPOSE 9001
ENTRYPOINT ["/entrypoint.sh"]
