#!/bin/bash

APP_ROOT=/opt/app-root/src
PROTECTED_PADS=(adboard board codeofconduct edsearch marketing releaseteam boardonly governance finance exec-board-only)

for pad_name in "${PROTECTED_PADS[@]}"; do
    echo "${pad_name}:${pad_name}_to_replace" > $APP_ROOT/$pad_name.htaccess
    if [[ `env | grep "^${pad_name}_SECRET" | cut -d'=' -f1 | cut -d'_' -f1` == "$pad_name" ]]; then
        secret=`env | grep "^${pad_name}_SECRET" | cut -d'=' -f2`
        sed -i "s|${pad_name}_to_replace|${secret}|" $APP_ROOT/$pad_name.htaccess
    fi
done
